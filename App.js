import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity, ScrollView } from "react-native";
import InputTextField from "./components/InputTextField";

export default class App extends React.Component {
    render() {
        return (
            <ScrollView style={styles.container}>
                <View>
                    <View style={{ marginTop: 60, alignItems: "center", justifyContent: "center" }}>
                        <Image source={require("./assets/react.png")} style={ {width: 100, height:100}}/>
                        <Text style={[styles.text, { marginTop: 10, fontSize: 22, fontWeight: "500" }]}>React Lab 02</Text>
                    </View>
                    <View style={{ marginTop: 48, flexDirection: "row", justifyContent: "center" }}>
                        <TouchableOpacity>
                            <View style={styles.socialButton}>
                                <Image source={require("./assets/facebook.png")} style={styles.socialLogo} />
                                <Text style={styles.text}>Facebook</Text>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.socialButton}>
                            <Image source={require("./assets/google.png")} style={styles.socialLogo} />
                            <Text style={styles.text}>Google</Text>
                        </TouchableOpacity>
                    </View>

                    <Text style={[styles.text, { color: "#ABB4BD", fontSize: 15, textAlign: "center", marginVertical: 20 }]}>---O---</Text>

                    <InputTextField style={styles.inputTitle} title="Email" />
                    <InputTextField
                        style={{
                            marginTop: 32,
                            marginBottom: 8
                        }}
                        title="Contraseña"
                        isSecure={true}
                    />

                    <Text style={[styles.text, styles.link, { textAlign: "right" }]}>¿Olvidaste tu contraseña?</Text>

                    <TouchableOpacity style={styles.submitContainer}>
                        <Text
                            style={[
                                styles.text,
                                {
                                    color: "#FFF",
                                    fontSize: 16
                                }
                            ]}
                        >
                            Iniciar Sesión
                        </Text>
                    </TouchableOpacity>

                    <Text
                        style={[
                            styles.text,
                            {
                                fontSize: 14,
                                color: "#ABB4BD",
                                textAlign: "center",
                                marginTop: 24
                            }
                        ]}
                    >
                        ¿ No tienes una cuenta ? <Text style={[styles.text, styles.link]}>Registrate ahora</Text>
                    </Text>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        paddingHorizontal: 30
    },
    text: {
        fontFamily: "Avenir Next",
        color: "#1D2029"
    },
    socialButton: {
        flexDirection: "row",
        marginHorizontal: 12,
        paddingVertical: 12,
        paddingHorizontal: 30,
        borderRadius: 4,
        backgroundColor: "#fff",
        elevation: 5
    },
    socialLogo: {
        width: 16,
        height: 16,
        marginRight: 8
    },
    link: {
        color: "#53c1de",
        fontSize: 14,
        fontWeight: "500"
    },
    submitContainer: {
        backgroundColor: "#53c1de",
        fontSize: 16,
        borderRadius: 4,
        paddingVertical: 12,
        marginTop: 32,
        alignItems: "center",
        justifyContent: "center",
        color: "#FFF",
        elevation: 5
    }
});
